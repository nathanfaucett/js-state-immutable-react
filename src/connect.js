var React = require("react"),
    immutable = require("immutable"),
    isArray = require("@nathanfaucett/is_array"),
    isFunction = require("@nathanfaucett/is_function"),
    arrayForEach = require("@nathanfaucett/array-for_each"),
    inherits = require("@nathanfaucett/inherits"),
    extend = require("@nathanfaucett/extend"),
    defaultMapping = require("./mapping"),
    defaultShouldUpdate = require("./shouldUpdate");

var Map = immutable.Map;

module.exports = connect;

function connect(stores, options) {
    var mapping, shouldUpdate;

    options = options || {};

    if (!isArray(stores)) {
        throw new TypeError(
            "connect(stores[, mapping[, shouldUpdate]]) stores must be an Array"
        );
    }

    mapping = isFunction(options.mapping) ? options.mapping : defaultMapping;
    shouldUpdate = isFunction(options.shouldUpdate)
        ? options.shouldUpdate
        : defaultShouldUpdate;

    function getNextState() {
        var localStores = stores;

        return Map().withMutations(function mutations(state) {
            arrayForEach(localStores, function each(store) {
                state.set(store.name(), store.state());
            });

            return state;
        });
    }

    function updateIfNeeded(_this) {
        var nextState = getNextState(),
            shouldUpdateResult;

        if (_this._isMounted) {
            shouldUpdateResult = shouldUpdate.call(
                _this.componentRef.current,
                _this._state,
                nextState,
                _this.props
            );

            _this._state = nextState;

            if (shouldUpdateResult !== false) {
                _this.forceUpdate();
            }
        }
    }

    return function connectComponent(Component) {
        var ConnectPrototype;

        if (!isFunction(Component)) {
            throw new TypeError(
                "connect(...)(Component) Component should be a Function"
            );
        }

        function Connect(props) {
            var _this = this,
                isUpdating = false;

            React.Component.call(this, props);

            this.componentRef = React.createRef();
            this._state = getNextState();
            this._isMounted = false;

            this._onUpdate = function onUpdate() {
                if (!isUpdating) {
                    isUpdating = true;

                    process.nextTick(function onNextTick() {
                        isUpdating = false;
                        updateIfNeeded(_this);
                    });
                }
            };

            arrayForEach(stores, function each(store) {
                store.on("update", _this._onUpdate);
            });
        }
        inherits(Connect, React.Component);
        ConnectPrototype = Connect.prototype;

        Connect.displayName =
            "Connect(" +
            (Component.displayName || Component.name || "Component") +
            ")";

        ConnectPrototype.componentDidMount = function() {
            this._isMounted = true;
        };

        ConnectPrototype.componentWillUnmount = function() {
            var _this = this;

            arrayForEach(stores, function each(store) {
                store.off("update", _this._onUpdate);
            });

            this._isMounted = false;
        };

        ConnectPrototype.render = function() {
            return React.createElement(
                Component,
                extend(
                    { ref: this.componentRef },
                    this.props,
                    mapping(this._state, this.props)
                )
            );
        };

        return Connect;
    };
}
