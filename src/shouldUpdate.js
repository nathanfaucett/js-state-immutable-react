module.exports = shouldUpdate;

function shouldUpdate(prevState, nextState) {
	return !prevState.equals(nextState);
}
